<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('users', UserController::class)->middleware('auth:api');
Route::resource('products', ProductController::class)->middleware('auth:api');
Route::middleware('auth:api')->get('/orders', [OrderController::class, 'index']);
// register
Route::post('/register', [AuthController::class, 'register']);
// login
Route::post('/login', [AuthController::class, 'login']);
