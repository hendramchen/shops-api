<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'admin@test.com',
                'password' => bcrypt('secret'),
                'is_admin' => 1,
                'is_warehouse' => 0,
                'status' => 'active',
            ],
            [
                'email' => 'warehouse@test.com',
                'password' => bcrypt('secret'),
                'is_admin' => 0,
                'is_warehouse' => 1,
                'status' => 'active',
            ],
            [
                'email' => 'customer@test.com',
                'password' => bcrypt('secret'),
                'is_admin' => 0,
                'is_warehouse' => 0,
                'status' => 'active',
            ],
            [
                'email' => 'hendra@test.com',
                'password' => bcrypt('secret'),
                'is_admin' => 0,
                'is_warehouse' => 0,
                'status' => 'active',
            ],
        ]);
    }
}
