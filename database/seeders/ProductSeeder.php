<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'name' => 'Xiaomi Mi A2',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/237/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 300,
                'status' => 'active'
            ],
            [
                'name' => 'Vivo ABC',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/239/200/300',
                'cpu' => 2,
                'ram' => 3,
                'price' => 200,
                'status' => 'active'
            ],
            [
                'name' => 'iPhone X',
                'os' => 'ios',
                'photo' => 'https://picsum.photos/id/240/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 700,
                'status' => 'active'
            ],
            [
                'name' => 'iPhone 6',
                'os' => 'ios',
                'photo' => 'https://picsum.photos/id/241/200/300',
                'cpu' => 2,
                'ram' => 3,
                'price' => 300,
                'status' => 'active'
            ],
            [
                'name' => 'Samsung Galaxy X',
                'os' => 'android',
                'photo' => 'https://picsum.photos/id/242/200/300',
                'cpu' => 2,
                'ram' => 4,
                'price' => 500,
                'status' => 'active'
            ],
        ]);
    }
}
